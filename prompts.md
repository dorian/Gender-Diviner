# Prompts/Brainstorm

Your favorite tradition is...
    - Holiday movies and hot cocoa
    warm, cozy, and heartwarming
    - Costumes and Candy
    sweet and theatrical
    - Your morning cup of coffee
    quiet and reflective
    - Summer road trip
    bright and adventurous
    - Making smores over a fire while camping in the middle of nowhere
    peaceful and serene
    - Telling ghost stories when the power goes out
    thrilling yet scary
    - Serving a hot meal at a soup kitchen
    kind, open, and big hearted

You see someone at a cafe with the same hair color/style as your ex. It reminds you of the smell of...
    - first cup of coffee on Saturday morning
    life giving and invigorating
    - freshly cut grass
    active and sweaty
    - Cinnamon and Clove
    sweet and warm
    - disinfectant
    clinical and realistic
    - over ripe fruits in the fridge
    sentimental and forgetful
    - vanilla and cardamum
    festive and celebratory
    - sulfur and disappointment
    mournful perhaps

You pull your favorite shirt from the dryer, it's getting too worn to wear. What will you do with it??
    - Patch more holes, soon it'll be more patch than shirt
    practical and thrifty
    - Make a throw pillow out of it
    crafty and quirky
    - Throw it out, now I have room for more shirts!
    efficient and exploratory
    - leave it in my closet
    sentimental and soft
    - wear it anyways
    unconcerned with pretense
    - give it to my pet to lay on
    the warmth of a companion

You keep your diary from prying eyes by...
    - full gpg encryption
    nerdy and comprehensive
    - an old fashioned lock and key
    well worn like a favorite sweater
    - verbal threats
    sharp and threatening
    - burn after writing
    tender and cautious
    - hiding it under the mattress, next to my porn
    timid yet thrill seeking
    - never writing it down to begin with
    guarded to a fault
    - performing it on stage as a monologue
    bold and expressive

Your dream childhood pet was a...
    - a soft shelled turtle
    gentle and protective
    - fluffy and wide eyed corgi puppy
    sweet and innocent
    - a painted rock, it's perfect because I made it so.
    quirky mastermind
    - a soft calico
    sassy and confident
    - a unicorn!
    creator of your own world
    - rambunctious thieving ferret
    wild at heart, small but mighty

